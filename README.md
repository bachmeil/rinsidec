### About

This is a slightly modified version of the [RInside](http://dirk.eddelbuettel.com/code/rinside.html) package.
The only modifications are that:

- The current version is the RInside code that was pulled on September 11, 2016 from the [Github repo](https://github.com/eddelbuettel/rinside). This prevents against breakage if RInside is updated.
- I have added a C interface. *The only reason to use this package rather than RInside is because you want a C interface rather than a C++ interface to R*. When your program links to libRInside.so, there is a running R interpreter available, so don't use this with C++ (although any RInside program will work with this library too).

I created this to embed R inside D programs. It should work with any language that has a C FFI, though I have only used it with D.

### Functions

These are the functions that I've added (you will see them at the bottom of src/RInside.cpp):

```
void passToR(SEXP x, char * name)
SEXP evalInR(char * cmd)
void evalQuietlyInR(char * cmd)
```

`passToR` takes a SEXP struct and the name of the variable as arguments. `x` is passed into R with the name `name`.

`evalInR` takes a C string holding code and evaluates it in R. You can send any valid R code. It returns the output in the form of a SEXP struct. A common use for this it to pass the name of an R variable, which returns the corresponding SEXP struct to be used in C.

`evalQuietlyInR` takes a C string holding code, evaluates it in R, and returns nothing.

If you do not know what SEXP structs are you can read about the R API [here](http://adv-r.had.co.nz/C-interface.html). Be forewarned that it can be messy working with strings in C.

### License

GPL (>= 2)
